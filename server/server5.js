/**
 * A friendly server that responds to URL requests with html after calling another service
 * Try: localhost:8085/Maryville, Missouri
 */
const http = require("http")
const temperatureService = require('./TemperatureService.js')
const port = 8085

const server = http.createServer(async (req, res) => {
    const city = await getCityFromRequest(req)
    if (city !== 'favicon.ico') {
        const ans = await temperatureService.get(city)
        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.write("<!doctype html>")
        res.write("<html><head><title>Server4</title></head>")
        res.write("<body>")
        res.write("<h1>Hello Client: Enter a city in the URL to get the weather</h1>")
        res.write("<h1>You made a GET weather request for " + city + ".<h1>")
        res.write("<p>" + ans + "<p>")
        res.write("<p>Please try again.<p>")
        res.end("</body></html>")
    }
})

async function getCityFromRequest(request) {
    const { headers, method, url } = request
    const strippedURL = url.substring(1, url.length) // remove the slash at url[0]
    const city = strippedURL.replace('%20', " ")
    return city
}

server.listen(port, (err) => {
    if (err) { return console.log('Error', err) }
    console.log(`Server listening at http://127.0.0.1:${port}`)
})

  